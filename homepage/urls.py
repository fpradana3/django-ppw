from django.contrib import admin
from django.urls import path
from .views import(
    index,
    schedule_create,
    schedule,
    schedule_delete,
    schedule_detail

)



app_name = 'homepage'

urlpatterns = [
    path('', index, name='index'),
    path('create/', schedule_create.as_view(success_url='/schedule/'),name='form' ),
    path('schedule/',schedule.as_view(),name='schedule_list'),
    path('schedule/<int:pk>/delete/',schedule_delete.as_view(success_url='/schedule/'),name='schedule_delete'),
    path('schedule/<int:pk>', schedule_detail, name='schedule_detail')
]