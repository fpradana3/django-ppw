from django.shortcuts import render
from django.views.generic import DeleteView,ListView, DetailView, CreateView
from .models import Schedule
from .forms import ScheduleForm

# Create your views here.
def index(request):
    return render(request, 'index.html')

    
class schedule_create(CreateView):
    model = Schedule
    form_class = ScheduleForm
    template_name = 'CreateSchedule.html'
    context = 'form'
    
class schedule(ListView):
    model = Schedule
    qs = Schedule.objects.all()
    context_obj_name = 'Schedule'
    template_name = "jadwal.html"

class schedule_delete(DeleteView):
    model = Schedule
    template_name = 'delete.html'

class schedule_detail(DetailView):
    model = Schedule
    template_name = 'detail.html' 
