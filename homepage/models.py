from django.db import models
from django.urls import reverse

# Create your models here.
class Schedule(models.Model):
    
    day_choices = [
        ('Senin','Senin'),
        ('Selasa','Selasa'),
        ('Rabu','Rabu'),
        ('Kamis','Kamis'),
        ('Jumat','Jumat'),
        ('Sabtu','Sabtu'),
        ('Minggu','Minggu')
    ]

    category_choices = [
        ('Study','Study'),
        ('Event', 'Event'),
        ('Work', 'Work'),
        ('Gaming', 'Gaming'),
        ('Sport', 'Sport'),
        ('Organization', 'Organization')
    ]

    activity = models.CharField(max_length=100)
    place = models.CharField(max_length=100)
    day = models.CharField(max_length=10,choices=day_choices,default='Sun')
    date = models.DateTimeField(auto_now=False, auto_now_add=False)
    category = models.CharField(max_length=20,choices=category_choices,default='St')    
    class Meta:
        ordering = ['-date']

    def get_absolute_url(self):
        return reverse('schedule_detail', kwargs={'pk': self.pk})


# Create your models here.
